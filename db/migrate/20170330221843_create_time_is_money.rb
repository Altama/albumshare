class CreateTimeIsMoney < ActiveRecord::Migration
  def self.up
    create_table :timeismoney_accounts do |t|
      t.string    :identifier
      t.integer   :billing_day
      t.integer   :billing_month
      t.timestamps
    end

    create_table :timeismoney_billable_features do |t|
      t.string     :account_identifier
      t.belongs_to :account
      t.integer    :activation_price
      t.integer    :price_per_day
      t.string     :feature_identifier
      t.string     :display_name
      t.date       :init_enabled_on
      t.date       :last_enabled_on
      t.date       :disabled_on
      t.boolean    :deactivated
      t.timestamps
    end
    
    create_table :timeismoney_invoices do |t|
      t.belongs_to :account
      t.string     :identifier
      t.string     :account_identifier
      t.string     :billing_type
      t.date       :billing_date
      t.timestamps
    end
    
    create_table :timeismoney_invoice_details do |t|
      t.belongs_to :invoice
      t.string     :invoice_identifier
      t.string     :billing_type
      t.integer    :amount_millicents
      t.string     :feature_identifier
      t.string     :feature_name
      t.integer    :days_used
      t.date       :disabled_on
      t.timestamps
    end

    create_table :timeismoney_payments do |t|
      t.belongs_to :account
      t.string     :identifier
      t.string     :account_identifier
      t.string     :invoice_identifier
      t.text       :data
      t.boolean    :paid
      t.date       :payment_date
      
      t.timestamps
    end

  end
  def self.down
    drop_table :timeismoney_payments
    drop_table :timeismoney_invoice_details
    drop_table :timeismoney_invoices
    drop_table :timeismoney_billable_features
    drop_table :timeismoney_accounts
  end
end


class AddAlbums < ActiveRecord::Migration[5.0]
  def change
    create_table :albums do |t|
      t.string :type
      t.references :user
      t.string :google_drive_file_id
      t.boolean :shared, default: false
      t.timestamps
    end
  end
end

class AddDropboxSessionToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :dropbox_session, :string
  end
end

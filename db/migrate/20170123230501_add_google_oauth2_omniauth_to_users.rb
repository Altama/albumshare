class AddGoogleOauth2OmniauthToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :google_oauth2_uid, :string
  end
end

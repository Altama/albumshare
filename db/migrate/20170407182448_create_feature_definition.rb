class CreateFeatureDefinition < ActiveRecord::Migration[5.0]
  def change
    create_table :feature_definitions do |t|
      t.string :name
      t.belongs_to :feature, foreign_key: true
      t.decimal :price_per_day
      t.decimal :addition_price
    end
  end
end

class CreateStripeCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :stripe_customers do |t|
      t.belongs_to :user, foreign_key: true
      t.string :customer_id

      t.timestamps
    end
  end
end

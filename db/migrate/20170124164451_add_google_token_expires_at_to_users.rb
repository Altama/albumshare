class AddGoogleTokenExpiresAtToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :google_token_expires_at, :string
  end
end

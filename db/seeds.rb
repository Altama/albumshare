# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin = User.create({
    email: "admin@altama.ca",
    password: "password",
    password_confirmation: "password",
    is_admin: true,
    first_name: "Admin",
    last_name: "Istrator",
    company_name: "Altama",
    contact_email: "admin@altama.ca"
})

[
  { identifier: "upgrade.unlimited_galleries", name: "Unlimited Galleries" },
  { identifier: "upgrade.multiple_sources", name: "Multiple Gallery Sources" },
  { identifier: "upgrade.private_galleries", name: "Private Galleries" },
  { identifier: "upgrade.raw_files", name: "RAW File Support" },
  { identifier: "upgrade.exif_info", name: "EXIF Info Support" },
  { identifier: "upgrade.watermark", name: "Custom Watermarks" },
  { identifier: "upgrade.link_aliasing", name: "Sharable Link Aliasing" },
  { identifier: "upgrade.profile_enhancements", name: "Profile Enhancements" },
  { identifier: "upgrade.panorama_images", name: "Panorama Image Support" },
  { identifier: "upgrade.image_tools", name: "Image Editing Tools" },
  { identifier: "upgrade.albumshare_cloud", name: "Albumshare Cloud" },
  { identifier: "upgrade.audio_slideshow", name: "Audio Slideshows" }
].each do |hash|
  Feature.create(hash)
end

Feature.all.each do |feature|
  puts feature.name
  FeatureDefinition.create({
    name: feature.name,
    feature: feature,
    price_per_day: 6666,           # 0.06 per day ≈ 2.00 a month
    addition_price: 200000         # 2.00 to add
  })
end
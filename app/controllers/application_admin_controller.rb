class ApplicationAdminController < ApplicationController
  before_action :ensure_admin!

  private

  def ensure_admin!
    unless current_user.admin?
      flash_notice "not_admin"
      redirect_to root_path
      return false
    end
  end
end
class HomeController < ApplicationController
  skip_authorization_check
  skip_before_action :authenticate_user!

  layout 'marketing'
  def index
   render 'marketing/index'
  end
  def about
   render 'marketing/about'
  end
  def cost
   render 'marketing/cost'
  end
  def awesome
   render 'marketing/awesome'
  end
end

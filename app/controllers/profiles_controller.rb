class ProfilesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index, :show]

  def show
    load_user(:show_profile)
    @features = Feature.all
    @enabled_feature_identifiers = current_user&.enabled_feature_identifiers || []
  end

  def edit
    load_user(:update)
  end

  def update
    load_user(:update)
    if @user.update(user_params)
      flash.now[:notice] = "Changes saved!"
      redirect_to profile_path(@user) and return
    end
    render :show
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :company_name, :contact_email, :bio)
  end

  private

  def load_user(for_action)
    @user = User.find(params[:id])
    authorize! for_action, @user
  end


end

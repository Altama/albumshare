class FeaturesController < ApplicationController
  load_and_authorize_resource
  skip_before_action :authenticate_user!, except: [:enable, :disable]

  def index
    enabled_features = current_user&.enabled_features || []
    current_features = enabled_features.select { |f| f["state"]=="current" }
    grace_features = enabled_features.select { |f| f["state"]=="grace" }

    @current_feature_identifiers = current_features.map { |f| f["feature_identifier"] }
    @grace_feature_identifiers = grace_features.map { |f| f["feature_identifier"] }
    @enabled_feature_identifiers = enabled_features.map { |f| f["feature_identifier"] }
  end
  
  def show
  end

  def enable
    if current_user.stripe_customer.nil?
      redirect_to customer_path and return
    end
    
    feature_definition = feature_definition_from_feature

    invoice = TimeIsMoney.add_billable_feature_to_account(
      account_identifier: current_user.timeismoney_account,
      **feature_definition.to_billable_feature_data
    )

    account = current_user.account
    begin
      charge = current_user.stripe_customer.charge(invoice["total_cents"])
      account.invoicing_succeeded(invoice["identifier"])
    rescue Stripe::StripeError => e
      account.invoicing_failed(invoice["identifier"])
      render text: e.json_body and return
    end

    flash_notice "time_is_money.feature_enabled", feature_name: @feature.name
    redirect_to features_path
  end

  def reenable
    raise ActiveRecord::NotFound unless current_user.has_feature(@feature.identifier)
    
    invoice = TimeIsMoney.enable_feature_on_account(
      account_identifier: current_user.timeismoney_account,
      feature_identifier: @feature.identifier)

    unless invoice.empty?
      account = current_user.account
      begin
        charge = current_user.stripe_customer.charge(invoice["total_cents"])
        account.invoicing_succeeded(invoice["identifier"])
      rescue Stripe::StripeError => e
        account.invoicing_failed(invoice["identifier"])
        render text: e.json_body and return
      end
      flash_notice "time_is_money.feature_reenabled_with_charge", 
        feature_name: @feature.name, 
        charge: number_to_currency(cents_to_dollars(invoice["total_cents"]))
    end

    flash_notice "time_is_money.feature_reenabled_without_charge", feature_name: @feature.name
    redirect_to features_path
  end

  def disable
    if current_user.stripe_customer.nil?
      redirect_to customer_path and return
    end
    
    feature_definition = feature_definition_from_feature
    account = current_user.account
    account.disable_feature(@feature.identifier)

    flash_notice "time_is_money.feature_disabled", feature_name: @feature.name
    redirect_to features_path
  end

  private

  def feature_definition_from_feature
    @feature.feature_definitions.first
  end

end
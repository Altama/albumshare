require "google_drive"
require "googleauth"
require "yaml"

class GoogleDriveController < ApplicationController
  skip_before_action :authenticate_user!
  skip_authorization_check

  before_action :set_credentials
  def set_credentials
    @credentials = Google::Auth::UserRefreshCredentials.new(
      client_id: ENV["GOOGLE_APP_ID"],
      client_secret: ENV["GOOGLE_APP_SECRET"],
      scope: ["https://www.googleapis.com/auth/drive"],
      redirect_uri: google_drive_callback_url
    )
  end

  def unauthorize
    session[:google_token] = nil
    current_user.google_token = nil
    current_user.save!
  end
 
  def authorize
    credentials = @credentials
    # add additional parameters after the first instance of '&' and redirect.
    # credentials sets the url automatically with pre-set params, so we add here.
    redirect_to credentials.authorization_uri.to_str.sub("&", "&prompt=consent&select_account=true&") 
  end

  def google_drive_callback
    credentials = @credentials

    # retrieve code upon success (if no code, error 404, auth failed)
    render status: 404 and return if params[:code].blank?

    # get token
    credentials.code = params[:code]
    credentials.fetch_access_token!
    session = GoogleDrive::Session.from_credentials(credentials)

    # serialize the google token and save to db
    # NOTE: tokens only last for 1 hour, token_refresh can be used to circumvent this
    current_user.update_attributes(:google_token => YAML::dump(session))
    # save refresh token and expires_at to db
    current_user.update_attributes(:google_refresh_token => YAML::dump(credentials.refresh_token))
    current_user.update_attributes(:google_token_expires_at => YAML::dump(credentials.expires_at))

    redirect_to root_path
  end 
end

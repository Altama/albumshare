class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  check_authorization unless: :devise_controller?
  before_action :authenticate_user!
  
  protected
  
  def flash_alert(translate_key_or_message, replacements={})
    flash[:alert] = t(translate_key_or_message, replacements)
  end
  
  def flash_notice(translate_key_or_message, replacements={})
    flash[:notice] = t(translate_key_or_message, replacements)
  end
  
  def flash_error(translate_key_or_message, replacements={})
    flash[:error] = t(translate_key_or_message, replacements)
  end
  
end

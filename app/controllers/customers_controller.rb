class CustomersController < ApplicationController
  skip_authorization_check

  def show
    @stripe_customer = current_user.stripe_customer
  end

  def edit
    @stripe_customer = current_user.stripe_customer
  end

  def update
    deleted = delete_existing_customer_at_stripe
    customer = create_customer_at_stripe

    # create StripeCustomer object
    current_user.create_stripe_customer(
      customer_id: customer.id
    )
    
    current_user.save

    if deleted
      flash_notice "stripe.customer_replaced"
    else
      flash_notice  "stripe.customer_saved"
    end
    redirect_to customer_path
  end

  def destroy
    if current_user.stripe_customer
      delete_existing_customer_at_stripe

      current_user.stripe_customer = nil
      current_user.save

      flash_notice "stripe.customer_removed"
    else
      flash_alert "stripe.customer_missing"
    end
    redirect_to customer_path
  end

  def stripe_params
    params.require(:stripeToken)
  end

  private
  def create_customer_at_stripe
    Stripe::Customer.create(
      :description => current_user.email,
      :email => current_user.email,
      :source => params[:stripeToken]
    )
  end
  def delete_existing_customer_at_stripe
    if current_user.stripe_customer
      customer = Stripe::Customer.retrieve(current_user.stripe_customer.customer_id)
      customer.delete
      true
    else
      false
    end
  end

end

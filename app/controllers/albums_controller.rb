class AlbumsController < ApplicationController
  skip_before_action :authenticate_user!
  skip_authorization_check
  
  before_action :set_albums_new
  def set_albums_new
    @user = current_user
    # initialize cloud services (deserialize saved session tokens)
    @cloud_storage = CloudStorageAdapter.new(@user)
  end
  
  def index
    # get all albums/populate the album list (@cloud_storage.albums)
    @cloud_storage.get_albums
  end

  def album_select
    # using the type, load up the images associated with the album that has
    # the identifier param. 
    @cloud_storage.get_album(params[:type], params[:identifier])
    respond_to do |format|
      format.js 
    end
  end
end


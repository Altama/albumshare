module Admin
  class FeaturesController < ApplicationAdminController
    before_action :load_definitions, only: :index
    load_and_authorize_resource
    
    def index
    end
    
    def edit
    end

    def update
      respond_to do |format|
        if @feature.update(feature_params)
          format.html { redirect_to admin_features_path, notice: 'Feature was successfully updated.' }
          format.json { render :index, status: :ok, location: [:admin, @feature] }
        else
          format.html { render :edit }
          format.json { render json: @feature.errors, status: :unprocessable_entity }
        end
      end
    end

    private

    def load_definitions
      @features = Feature.accessible_by(current_ability).order('identifier asc')
    end

    def feature_params
      params.require(:feature).permit(:name, :description)
    end

  end
end
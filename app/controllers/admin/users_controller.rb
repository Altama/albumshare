module Admin
  class UsersController < ApplicationAdminController

    load_resource

    def index
      authorize! :manage, User
    end

    def update
      authorize! :admin_update, @user
      @user.update! user_params
      flash[:notice] = "#{@user.email} was updated successfuly!"
      redirect_to :back
    end

    private

    def user_params
      params.require(:user).permit(:is_admin)
    end
  end
end
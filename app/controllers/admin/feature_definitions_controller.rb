module Admin
  class FeatureDefinitionsController < ApplicationAdminController
    before_action :load_definitions, only: :index
    load_and_authorize_resource
    
    def index
    end
    
    def show
    end

    def new
    end

    def create
      respond_to do |format|
        if @feature_definition.save
          format.html { redirect_to [:admin, @feature_definition], notice: 'Feature Definition was successfully created.' }
          format.json { render :show, status: :created, location: [:admin, @feature_definition] }
        else
          format.html { render :new }
          format.json { render json: @feature_definition.errors, status: :unprocessable_entity }
        end
      end
    end

    def edit
    end

    def update
      respond_to do |format|
        if @feature_definition.update(feature_definition_params)
          format.html { redirect_to [:admin, @feature_definition], notice: 'Feature Definition was successfully updated.' }
          format.json { render :show, status: :ok, location: [:admin, @feature_definition] }
        else
          format.html { render :edit }
          format.json { render json: @feature_definition.errors, status: :unprocessable_entity }
        end
      end
    end

    def destroy
      @feature_definition.destroy
      respond_to do |format|
        format.html { redirect_to admin_feature_definitions_url, notice: 'Feature Definition was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private

    def load_definitions
      @feature_definitions = FeatureDefinition.accessible_by(current_ability).includes(:feature).joins(:feature).order('features.name asc')
    end

    def feature_definition_params
      params.require(:feature_definition).permit(:name, :feature_id, :price_per_day, :addition_price)
    end

  end
end
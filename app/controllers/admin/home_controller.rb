module Admin
  class HomeController < ApplicationAdminController
    skip_authorization_check
    def index
    end
  end
end
require 'dropbox_sdk'

class DropboxController < ApplicationController
  skip_before_action :authenticate_user!
  skip_authorization_check

  def unauthorize
    session[:dropbox_session] = nil
    current_user.dropbox_session = nil
    current_user.save!
  end

  def authorize
    dbsession = DropboxSession.new(ENV["DROPBOX_APP_ID"], ENV["DROPBOX_APP_SECRET"])
    # serialize and save this DropboxSession
    session[:dropbox_session] = dbsession.serialize
    redirect_to dbsession.get_authorize_url url_for(:action => 'dropbox_callback')
  end

  def dropbox_callback
    dbsession = DropboxSession.deserialize(session[:dropbox_session])
    dbsession.get_access_token # authorized, request an access_token
    session[:dropbox_session] = dbsession.serialize
    # save session to current_user, get rid of session
    current_user.update_attributes(:dropbox_session => session[:dropbox_session])
    session.delete :dropbox_session
    redirect_to root_path
  end 
end

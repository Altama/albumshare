class ContactController < ApplicationController
  skip_authorization_check
  layout 'marketing'
  def index
   render 'contact/index'
  end



  def create
    message_params = params.require(:message).permit(:name, :email, :body)
    @message = Message.new message_params

    if @message.valid?
      redirect_to new_message_url, notice: "Message received, thanks!"
    else
      render :new
    end
  end

end

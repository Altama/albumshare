class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:google_oauth2]

  has_many :albums
  has_one :stripe_customer

  def full_name
    [first_name, last_name].join(' ')
  end

  def admin?
    is_admin
  end
  
  def unlimited_galleries
    has_feature("upgrade.unlimited_galleries")
  end

  def enabled_features
    account.active_features
  end
  
  def enabled_feature_identifiers
    account.active_feature_identifiers
  end
  
  def has_feature(feature_identifier)
    enabled_feature_identifiers.include? feature_identifier
  end

  def connect_omniauth(omniauth_hash)
    user_attributes = {}
    user_attributes["#{omniauth_hash.provider}_uid".to_sym] = omniauth_hash.uid
    if omniauth_hash.provider == "google_oauth2"
      user_attributes[:google_token] = omniauth_hash.credentials.token
      user_attributes[:google_token_expires_at] = omniauth_hash.credentials.expires_at
      user_attributes[:google_refresh_token] = omniauth_hash.credentials.refresh_token
    end
    update(user_attributes)
  end

  def self.from_omniauth(omniauth_hash)
    where("#{omniauth_hash.provider}_uid".to_sym => omniauth_hash.uid).first
  end

  def account
    if @_account.nil?
      if timeismoney_account.nil?
        self.timeismoney_account = TimeIsMoney.create_account
        save
      end
      @_account = Account.new(timeismoney_account)
    end
    @_account
  end


end

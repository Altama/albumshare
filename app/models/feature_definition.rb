class FeatureDefinition < ApplicationRecord
  belongs_to :feature

  def to_billable_feature_data
    {
      feature_identifier: feature.identifier, 
      display_name: name, 
      price_per_day: price_per_day, 
      activation_price: addition_price
    }
  end
end
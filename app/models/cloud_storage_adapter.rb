class CloudStorageAdapter
  
  # getters
  
  def multiple_sources_enabled
    # variable to determine if multiple sources are enabled to allow dropbox connection
    # default to true for now during testing, set to false to turn off dropbox.
    @multiple_source_enabled = true 
  end

  def albums
    @albums
  end  

  def photos
    @photos
  end

  def title
    @title
  end

  def dropbox
    @dropbox
  end

  def google_drive
    @google_drive
  end

  # c'tor

  def initialize(user)
    @user = user
    @title = nil
    @photos = Hash.new
    @albums = Hash.new

    if @user.google_token
      @google_drive = GoogleDriveModel.new(@user)
    end
   
    # assuming google drive is the default, if multiple sources is enabled,
    # load any other cloud service the user has linked.
    if self.multiple_sources_enabled and @user.dropbox_session
      @dropbox = DropboxModel.new(@user)
    end
  end

  # methods

  # Purpose: using any instantiated cloud services, this method gets
  #          all albums within a users root directory (for each cloud service linked).
  #          the method then populates the first album in the list with any associated 
  #          images upon the initial page loading.
  #          NOTE: meant to be called on index. 
  # Params:  nil
  # Returns: nil
  def get_albums
    if @user.google_token
      @google_drive.get_albums
    end

    if self.multiple_sources_enabled and @user.dropbox_session
      @dropbox.get_albums 
    end 
    
    # populate list with album names and the type of service it's using
    get_album_list

    # load photos into first album
    if @user.google_token
      @photos = @google_drive.albums[@google_drive.root]
      @title = @google_drive.root
    elsif self.multiple_sources_enabled and @user.dropbox_session
      @photos = @dropbox.albums[@dropbox.root]
      @title = @dropbox.title
    else
      # nothing is linked with the account
    end
  end

  # Purpose: using the type of cloud service that is associated with an album,
  #          this method will use the identifier given to load up any images within 
  #          that identified album. 
  # Params:  type :string, identifier :string <= defaults are nil if nothing is passed.
  # Returns: nil
  def get_album(type = nil, identifier = nil)
    case type
      when "google_drive"
        if @user.google_token
          @google_drive.get_album(identifier)
          @photos = @google_drive.albums[identifier]
          @title = identifier
        end
      when "dropbox"
        if @user.dropbox_session
          @dropbox.get_albums(identifier)
          @photos = @dropbox.albums[@dropbox.title]
          @title = @dropbox.title
        end
    end
  end

  # Purpose: recurses through any cloud serviced linked with the users
  #          account and populates an album list with the album name,
  #          and the type of album it is: google_drive, dropbox, etc.
  # Params:  nil
  # Returns: nil
  def get_album_list
    if @user.google_token
      @google_drive.albums.each do |k,v|
        if !v.empty?
          @albums[k] = "google_drive"
        end
      end
    end
    if self.multiple_sources_enabled and @user.dropbox_session
      @dropbox.albums.each do |k,v|
        if !v.empty?
          @albums[k] = "dropbox"
        end
      end
    end
  end
end

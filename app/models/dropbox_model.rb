require 'dropbox_sdk'
require 'base64'

class DropboxModel

  # getters

  def albums
    @albums
  end

  def root
    "Dropbox Root"
  end

  def title
    @title
  end

  # c'tor

  def initialize(user)
    @user = user
    get_token
  end

  # methods

  def get_token
    @token = DropboxClient.new(DropboxSession.deserialize(@user.dropbox_session), "dropbox")
  end

  def get_albums(path = self.root)
    @albums = Hash.new
    @photos = Hash.new
    # get the correct metadata based on the path given
    if path.include? self.root
      @metadata = @token.metadata('/') 
      @title = self.root 
    else
      @metadata = @token.metadata(path)
      @title = path.split('/').last
    end

    dropbox_grep(@title, @metadata)
  end

  # dropbox grep
  def dropbox_grep(name, metadata={}) 
    @photos = Hash.new 
    if metadata["contents"]
      metadata["contents"].each do |i|
        # get all images
        if i["mime_type"]
          if i["mime_type"].include? "image" 
            full_url = @token.media(i["path"])["url"]
            @photos[get_thumbnail(i["path"])] = full_url
            @albums[name] = @photos
          end
        end   
        # if a dir is found, recurse and continue
        if i["is_dir"] 
          if i["is_dir"] == true
            dropbox_grep(i["path"], @token.metadata(i["path"]))
          end    
        end 
      end
    end
  end 

  # TODO not all image types tested
  def get_thumbnail(path)
    thumb64 = Base64.encode64(@token.thumbnail(path, "xl"))
    @thumb =  "data: image/" + path.split('.').last + ";base64," + thumb64
    # TODO be able to deal with all image types, gif needs changing.
    #case path.split('.').last
    #  when "jpeg", "jpg"
    #    @thumb =  "data: image/jpg;base64," + thumb64
    #  when "png"
    #    @thumb =  "data: image/png;base64," + thumb64
    #  when "gif"
    #    @thumb =  "data: image/gif;base64," + thumb64
    #  when "bmp"
    #    @thumb =  "data: image/bmp;base64," + thumb64
    #end
  end
end

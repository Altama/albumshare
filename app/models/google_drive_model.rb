require "google_drive"
require "googleauth"
require "yaml"

class GoogleDriveModel

  # getters

  def root
    "Google Drive Root"
  end

  def albums
    @albums
  end

  # c'tor

  def initialize(user)
    @user = user
    get_token
  end

  # methods

  def get_token
    # deserialize the google token
    @session = YAML::load(@user.google_token)
  end

  def get_albums
    @albums = Hash.new
    # grep all
    google_drive_grep
  end

  def get_album(name)
    @albums = Hash.new
    # grep for all images in folder with the same name as param
    if name == self.root
      google_drive_grep(name)
    else
      google_drive_grep(name, @session.file_by_title(name))
    end
  end

  def google_drive_grep(name = self.root, directory = nil)
    photos = Hash.new # hash to contain thumbnails as well as full size image links
   
    # might fix the issue? 
    if name == self.root
      directory = @session.root_collection
    end

    directory.files.each do |file|

      # get all images from the current directory
      if !file.trashed == true and file.mime_type.include? "image" 
        photos[file.thumbnail_link] = file.web_content_link.sub("&export=download", "")
      end
      @albums[name] = photos
      # if a directory is found, recurse and continue
      if !file.trashed == true and file.mime_type.include? "folder"
        #if !directory.file_by_title(file.name).to_s.include? "string"
          google_drive_grep(file.name, directory.file_by_title(file.name))
        #end
      end

    end

  end
end

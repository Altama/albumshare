class StripeCustomer < ApplicationRecord
  belongs_to :user

  def charge(amount)
    Stripe::Charge.create(
      :amount => amount,
      :currency => "cad",
      :customer => customer_id
    )
  end
end

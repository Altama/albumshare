class Account
  attr_reader :identifier

  def initialize(identifier)
    @identifier = identifier
  end

  def user
    User.find_by!(timeismoney_account: account_identifier)
  end

  def active_feature_identifiers     
    TimeIsMoney.active_feature_identifiers(account_identifier: identifier)
  end

  def active_features
    TimeIsMoney.active_features(account_identifier: identifier)
  end

  def generate_invoice
    TimeIsMoney.generate_invoice(account_identifier: identifier)
  end

  def invoicing_succeeded(invoice_identifier, data={})
    TimeIsMoney.invoicing_succeeded(
      account_identifier: identifier, 
      invoice_identifier: invoice_identifier,
      data: data
    )
  end

  def invoicing_failed(invoice_identifier, data={})
    TimeIsMoney.invoicing_failed(
      account_identifier: identifier, 
      invoice_identifier: invoice_identifier,
      data: data
    )
  end

  def disable_feature(feature_identifier)
    TimeIsMoney.disable_feature_on_account(
      account_identifier: identifier, 
      feature_identifier: feature_identifier
    )
  end

  def self.bill_all_customers_with_stripe
    results = {successes:[],failures:[]}
    TimeIsMoney.accounts_due.each do |account|
      begin
        account = self.new(account["identifier"])
        invoice_hash = account.generate_invoice
        unless invoice["succeeded"]
          ## charge the customer associated with the account
          user = account.user
          stripe_customer = user.stripe_customer
          raise "No Stripe Information for #{user_from_account.email}!" unless stripe_customer
          ## in the amount of invoice["total_cents"]
          amount = invoice["total_cents"]
          begin
            charge = stripe_customer.charge(amount)
            account.invoicing_succeeded(invoice_identifier, charge.to_hash)
            results[:successes] << "Charged succeeded charging #{user_from_account.email} for #{amount} cents"
          rescue Stripe::StripeError => e
            account.invoicing_failed(invoice_identifier, charge.to_hash)
            results[:failures] << "Charged failed charging #{user_from_account.email} for #{amount} cents"
          end
        end 
      rescue ActiveRecord::RecordNotFound => e
        results[:failures] << "Could not find user with account #{account_identifier}"
      rescue Exception => e
        results[:failures] << "Error charging #{user_from_account.email} for #{amount} cents: " + e.to_s
      end
    end
    self.process_deactivations    
  end

  def self.process_deactivations
    TimeIsMoney.process_deactivations
  end

end
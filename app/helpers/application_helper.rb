module ApplicationHelper

  def millicents_to_cents(millicents)
    millicents/1000.0
  end

  def cents_to_dollars(cents)
    cents/100.0
  end

  def millicents_to_dollars(millicents)
    cents_to_dollars(millicents_to_cents(millicents))
  end

  def present(value, default)
    if value.present?
      value
    else
      default
    end
  end
end

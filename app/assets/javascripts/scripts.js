$(document).on('turbolinks:load', function() {
  $('#dropbox').hover(function() {
    $('#google').addClass('active');
    //  $('#google.active').fadeIn( 500);
  }, function() {
    //  $('#google.active').fadeOut( 500);
    $('#google').removeClass('active');
  });


  $(".panel-left").resizable({
    handleSelector: ".splitter",
    resizeHeight: false
  });

  $(".panel-top").resizable({
    handleSelector: ".splitter-horizontal",
    resizeWidth: false
  });


  $(document).on('click', '.albumName', function(){
    //$(this).nextAll(".albumDetails").first().toggleClass('maximize');
    $('.albumName').removeClass('active');
    $('.albumDetails').removeClass('maximize');
    $target = $(event.target);
    $target.toggleClass('active');
  })

  $(document).on('click', '.photo-link', function(){
    $('.photo-link').removeClass('on');
    $(this).toggleClass('on');
  })

  $(document).on('dblclick', '.photo-link', function(){
    $($(this).data('target')).modal('show');
  })

  var biggestHeight = "0";
  $("#panel-rightCSS").each(function(){
    if ($(this).height() > biggestHeight ) {
      biggestHeight = $(this).height()
    }
  });

  var leftBarHeight = $('#panel-rightCSS').height();
  $('#panels').css("height", leftBarHeight);
});

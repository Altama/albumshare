namespace :billing do
  desc "Bill all pending customers"
  task bill_customers: :environment do
    Account.bill_all_customers_with_stripe
  end

  desc "Process Feature Deactivations"
  task process_deactivations: :environment do
    Account.process_deactivations
  end
end

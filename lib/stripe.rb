class Stripe::TestCard
  attr_reader :number
  attr_reader :expectation
  attr_reader :success

  def initialize(number, expectation, success)
    @number, @expectation, @success = number, expectation, success
  end

  class << self
    def success_cards
      @success_cards ||= test_cards.select { |c| c.success }
    end
    def fail_cards
      @fail_cards ||= test_cards.select { |c| !c.success }
    end
    def test_cards
      @test_cards ||= generate_cards
    end
    def generate_cards
      @test_cards ||= CARD_EXPECTATIONS.map { |number, hash| self.new(number, hash[:expectation], hash[:success]) }
    end
  end

  CARD_EXPECTATIONS = {
    "4242424242424242" => { success: true, expectation: "Charge succeeds" },
    "4012888888881881" => { success: true, expectation: "Charge succeeds" },
    "4000056655665556" => { success: true, expectation: "Charge succeeds" },
    "5555555555554444" => { success: true, expectation: "Charge succeeds" },
    "5200828282828210" => { success: true, expectation: "Charge succeeds" },
    "5105105105105100" => { success: true, expectation: "Charge succeeds" },
    "378282246310005"  => { success: true, expectation: "Charge succeeds" },
    "371449635398431"  => { success: true, expectation: "Charge succeeds" },
    "6011111111111117" => { success: true, expectation: "Charge succeeds" },
    "6011000990139424" => { success: true, expectation: "Charge succeeds" },
    "30569309025904"   => { success: true, expectation: "Charge succeeds" },
    "38520000023237"   => { success: true, expectation: "Charge succeeds" },
    "3530111333300000" => { success: true, expectation: "Charge succeeds" },
    "3566002020360505" => { success: true, expectation: "Charge succeeds" },
    "4000000000000077" => { success: true, expectation: "Charge succeeds and funds will be added directly to your available balance (bypassing your pending balance)." },
    "4000000000000093" => { success: true, expectation: "Charge succeeds and domestic pricing is used (other test cards use international pricing). This card is only significant in countries with split pricing." },
    "4000000000009235" => { success: true, expectation: "Charge succeeds with a risk_level of elevated and placed into review." },
    "4000000000000010" => { success: false, expectation: "The address_line1_check and address_zip_check verifications fail. If your account is blocking payments that fail postal code validation, the charge is declined." },
    "4000000000000028" => { success: false, expectation: "Charge succeeds but the address_line1_check verification fails." },
    "4000000000000036" => { success: false, expectation: "The address_zip_check verification fails. If your account is blocking payments that fail postal code validation, the charge is declined." },
    "4000000000000044" => { success: false, expectation: "Charge succeeds but the address_zip_check and address_line1_check verifications are both unavailable." },
    "4000000000000101" => { success: false, expectation: "If a CVC number is provided, the cvc_check fails. If your account is blocking payments that fail CVC code validation, the charge is declined." },
    "4000000000000341" => { success: false, expectation: "Attaching this card to a Customer object succeeds, but attempts to charge the customer fail." },
    "4000000000000002" => { success: false, expectation: "Charge is declined with a card_declined code." },
    "4100000000000019" => { success: false, expectation: "Charge is declined with a card_declined code and a fraudulent reason." },
    "4000000000000127" => { success: false, expectation: "Charge is declined with an incorrect_cvc code." },
    "4000000000000069" => { success: false, expectation: "Charge is declined with an expired_card code." },
    "4000000000000119" => { success: false, expectation: "Charge is declined with a processing_error code." },
    "4242424242424241" => { success: false, expectation: "Charge is declined with an incorrect_number code as the card number fails the Luhn check." }
  }.freeze    
end

# EXAMPLE
#
# Stripe::Charge.create(
#   :amount => 200, 
#   :currency => "cad",
#   :source => {
#     :number    => Stripe::TestCard.success_cards.map(&:number).sample,
#     :object    => 'card',
#     :exp_month => '12',
#     :exp_year  => '2018'
#   },
#   :description => "Sample Test Charge"
# )
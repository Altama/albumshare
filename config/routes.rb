Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, controllers: {omniauth_callbacks: "omniauth_callbacks"}

  namespace :admin do
    root 'home#index'
    resources :users, only: [:index, :update]
    resources :feature_definitions
    resources :features
  end

  resources :profiles, only: [:show, :edit, :update]

  resources :albums, only: [:index, :destroy] do
    post :share, on: :collection
  end
   get 'awesome', to: 'home#awesome', as: 'awesome'
 get 'cost', to: 'home#cost', as: 'cost'
 get 'about', to: 'home#about', as: 'about'
 get 'contact', to: 'messages#new', as: 'new_message'
 post 'contact', to: 'messages#create', as: 'create_message'

  resources :features, only: [:index, :show] do
    member do
      post :enable
      post :disable
      post :reenable
    end
  end
  resource :customer, only: [:show, :edit, :update, :destroy]

  get '/dropbox_authorize' => 'dropbox#authorize', as: 'dropbox_authorize'
  get '/dropbox_unauthorize' => 'dropbox#unauthorize', as: 'dropbox_unauthorize'
  get '/dropbox_callback' => 'dropbox#dropbox_callback', as: 'dropbox_callback'

  get '/album_select' => 'albums#album_select', as: 'album_select'

  get '/google_drive_authorize' => 'google_drive#authorize', as: 'google_drive_authorize'
  get '/google_drive_unauthorize' => 'google_drive#unauthorize', as: 'google_drive_unauthorize'
  get '/google_drive_callback' => 'google_drive#google_drive_callback', as: 'google_drive_callback'

  root 'home#index'
end

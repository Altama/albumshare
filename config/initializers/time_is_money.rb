TimeIsMoney.configure do |config|
  TimeIsMoney::Repositories::ActiveRecord.table_name_prefix = "timeismoney_"
  config.repository = TimeIsMoney::ActiveRecordRepository.new
end
